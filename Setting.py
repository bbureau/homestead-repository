class Setting:
    def __init__(self, *args):
        if len(args) == 3: 
            self.Name=args[0]
            self.Description=args[1]
            self.Value=args[2]
        elif len(args) == 2:
            self.Name=args[0]
            self.Value=args[1]
        else:
            row=args[0]
            self.ID=row[0]
            self.Name=row[1]
            self.Description=row[2]
            self.Value=row[3]
