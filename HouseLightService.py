from dateRepository import *
from fileRepository import *
from FloorRepository import *
from guizero import *
import sqlite3
from RoomRepository import *
from ArduinoRepository import *
class HouseSetting:
    def __init__(self, row):
         self.ID=row[0]
         self.Name=row[1]
         self.Code=row[2]

class HouseLightService:
    def __init__(self, dbConnection, arduinoRepo):
         self.dbConnection=dbConnection
         self.ArduinoRepository=arduinoRepo
    def GetHouseSetting(self, id):
        conn = sqlite3.connect(self.dbConnection)
        c = conn.cursor()
        command="Select ID, Name, Code from HouseConfiguration Where ID="+id
        c.execute(command)
        rows=c.fetchall()
        if len(rows)==0:
            return None
        entry=HouseSetting(rows[0])
        return entry
    def LoadSettings(self, ids):
        self.ArduinoRepository.SendSerialData(0)
        if ids==None:
            return
        idList=ids.split(",")
        for id in idList:
            self.LoadSetting(id)
        self.ArduinoRepository.GetSerialData()

    def LoadSetting(self, code):
        self.ArduinoRepository.SendSerialData(code)

    