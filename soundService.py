from dateRepository import *
from fileRepository import *
from guizero import *
from homeController import *
from HouseLightService import *
from WeatherRepository import *
from SettingRepository import *
import os
os.add_dll_directory(os.getcwd())
os.add_dll_directory("C:\\Program Files\\VideoLAN\\VLC")

from  vlc import *
class SoundService:
    def __init__(self, dbConn, soundAssets):
         self.dbConnection=dbConn
         self.SoundRepo=soundRepo(self.dbConnection)
         self.SoundAssetFolder=soundAssets
    def playMusic(self):
        return self.SoundRepo.getRandomSoundOfType(1)
    def playSound(self):
        return self.SoundRepo.getRandomSoundOfType(2)
    def playTestSound(self):
        return self.SoundRepo.getSoundById(41)
    def playAmbient(self):
        return self._playSound(2, False)
    def playRandom(self):
        return self._playSound(None, True)
    def _playSound(self, typeId, doInc):
        sound=self.SoundRepo.getRandomSound(typeId)
        if sound==None:
            return None
        self.SoundRepo.logSound(sound)
        if (doInc==True):
            self.SoundRepo.incrementPlayDate(sound)
        return sound

class guiController:
    def __init__(self, dbConn, soundFolder, imgPath,iconPath, arduinoCode, backupArduino):
         self.dbConnection=dbConn
         self.SoundService=SoundService(self.dbConnection, soundFolder)
         self.SettingRepository=SettingRepository(self.dbConnection)
         self.WeatherApiKey=self.SettingRepository.GetSettingValue('WeatherAPIKey').Value
         self.WeatherRepository=WeatherRepository(self.WeatherApiKey, self.SettingRepository)
         self.ImgRepository=FileRepository(imgPath)
         self.ActivePhoto=None
         self.ArduinoCode=arduinoCode
         self.WelcomeMessage=None
         self.app=None
         self.ImagePath=imgPath
         self.IconPath=iconPath
         self.SoundApp=None
         self.NameTitle=None
         self.NameValue=None
         self.DescriptionTitle=None
         self.DescriptionValue=None
         self.StarringValue=None
         self.StarringTitle=None
         self.YearTitle=None
         self.YearValue=None
         self.TitleSize=14
         self.Font="Arial Black"
         self.ValueSize=12
         self.TitleColor="black"
         self.DescriptionColor="lightblue"
         self.soundFolder=soundFolder
         self.ActiveSound=None
         self.ActiveSoundFile=None
         self.Volume=1.0
         self.ArduinoRepository=ArduinoRepository(arduinoCode, backupArduino)
         self.HouseLightService=HouseLightService(dbConn, self.ArduinoRepository)
         self.CurrentWindow=None
         self.ActiveDescription=None
         self.DescriptionValue=None
         self.app = App(title="Welcome Home", width=600)
         topbox=Box(self.app)
         self.WelcomeMessage = Text(topbox, text="Welcome Home", size=20, font="Arial Black", color="lightblue", align="left")
         self.TopSpacer=Text(topbox, text="   ", size=20, align="left")
         temp=self.WeatherRepository.GetTempF()
         color=self.WeatherRepository.GetTempColor(temp)
         if temp!=None:
             temp=str(temp)+"°F"
         else:
             temp=""
         self.Temperature=Text(topbox, text=temp,size=20, font="Arial Black", color=color, align="right")
         self.ActivePhoto=Picture(self.app, height=300)
         self.MarqueCount=0
         box=Box(self.app)
         iconPathed=iconPath+"\\"
         update_text = PushButton(box, command=self.playMusic, image=iconPathed+"noun_vinyl_689313.png", height=50, width=50, align="left")
         playSoundBtn = PushButton(box, command=self.playSound, image=iconPathed+"noun_Bird_1711227.png",height=50, width=50, align="left")
         detailsBtn= PushButton(box, command=self._loadHomeExplorer, image=iconPathed+"noun_House_1354102.png", align="left", height=50, width=50,)
         testBtn= PushButton(box, command=self._playTestSound, image=iconPathed+"noun_House_1354102.png", align="left", height=50, width=50,)
    
         VolumeTitle=Text(self.app, text="Volume", size=12, font=self.Font, color=self.TitleColor)
         self.Slider=Slider(self.app, command=self.changeVolume, start=0, end=100)
         self.Slider.value=100
         self.SecondarySlider=None

    def changeVolume(self):
        if self.ActiveSound!=None:
            vol=self.ActiveSoundFile.CustomVolume*self.Slider.value
            self.ActiveSound.audio_set_volume(vol)

    def playMusic(self):
       sound = self.SoundService.playMusic()
       if sound!=None:
           self._loadSound(sound)
           self._playSound(sound)

    def _playTestSound(self):
        sound=self.SoundService.playTestSound()
        if sound!=None:
           self._loadSound(sound)
           self._playSound(sound)



    
    def playSound(self):
       sound = self.SoundService.playSound()
       if sound!=None:
           self._loadSound(sound)
           self._playSound(sound)

    def _playSound(self, sound):
            self.ActiveSoundFile=sound
            path=self.soundFolder+"\\"+sound.Folder+"\\"+sound.FileName
            self.ActiveSound = MediaPlayer(path)
            weight=sound.CustomVolume*self.Slider.value
            self.ActiveSound.audio_set_volume(weight)
            self.ActiveSound.play()

    def _loadSound(self, sound):
        window=self._setupPlayMusic(sound)
        window.show()
        self.CurrentWindow=window
        if sound.HouseConfigCodes!=None:
            if sound.HouseConfigLeadTime!=None:
                self.CurrentWindow.after(sound.HouseConfigLeadTime*1000+1000, self.HouseLightService.LoadSettings, args=sound.HouseConfigCodes)
                return
            self.HouseLightService.LoadSettings(sound.HouseConfigCodes)
        

    def _loadRandomSound(self):
        if self.ActiveSound!=None and self.ActiveSound.get_state()==State.Playing:
            return

        sound = self.SoundService.playRandom()
        if sound!=None:
           self._loadSound(sound)
           self._playSound(sound)
           self.CurrentWindow.after(self.ActiveSound.get_length(), self._clearPlayer)

    def _clearPlayer(self):
        self.CurrentWindow.destroy()
        self.app.show()
        self.ActiveSound.stop()
        self.ActiveSound=None
        self.ActiveSoundFile=None

    def _loadHomeExplorer(self):
        app=HomeController(self.dbConnection, self.ImagePath, self.IconPath, self.app, self.ArduinoCode, self.ArduinoRepository),

    def _loadCircaYear(self, sound):
        current_time = datetime.datetime.today() 
        month=str(current_time.month)
        if len(month)<2:
             month='0'+str(month)

        day=str(current_time.day)
        if len(day)<2:
            day='0'+str(day)

        defaultDate=month+"/"+day+"/1987"
        if sound==None or sound.AirDate==None:
            return defaultDate
        
        if sound.UseNativeMonthAndDay!=1:
            return sound.AirDate
        
        parts=sound.AirDate.split("-")
        if len(parts)<3:
            return defaultDate
        result=month+"/"+day+"/"+str(parts[2])
        return result

    def _setPrimarySliderVal(self):
        self.Slider.value=self.SecondarySlider.value
        self.changeVolume()

    def _setupPlayMusic(self, sound):
        result=Window(self.app, title=sound.Name)
        starringTitle=Text(result, text="Starring:", size=self.TitleSize, font=self.Font, color=self.TitleColor)
        starringValue=Text(result, text=sound.Stars, size=self.ValueSize, font=self.Font, color=self.DescriptionColor)
        yearTitle=Text(result, text="Airdate:", size=self.TitleSize, font=self.Font, color=self.TitleColor)
        yearValue=Text(result, text=self._loadCircaYear(sound), size=self.ValueSize, font=self.Font, color=self.DescriptionColor)
        descriptionTitle=Text(result, text="Abstract:", size=self.TitleSize, font=self.Font, color=self.TitleColor)
        self.ActiveDescription=sound.Description
        if sound.Description!=None:
            self.setupDescription(sound, result)

        
        VolumeTitle=Text(result, text="Volume", size=12, font=self.Font, color=self.TitleColor)
        self.MarqueCount=0
        self.SecondarySlider=Slider(result, 0,100,command=self._setPrimarySliderVal)
        self.SecondarySlider.value=self.Slider.value

        result.on_close(self._clearPlayer)
        # self.DescriptionValue.repeat(250, self.updateTitle)
        return result
    def setupDescription(self, sound, result):
        if sound.Description==None:
            return
        if (len(sound.Description)<51):
            self.setupDescription=Text(result, text=sound.Description, size=10, font=self.Font, color=self.DescriptionColor, width=45)
            return
        
        lenVal=len(sound.Description)
        startV=51
        endV=51+50
        self.DescriptionValue=Text(result, text=sound.Description[0:50], size=10, font=self.Font, color=self.DescriptionColor, width=45)
        while startV<=lenVal:
            if endV>lenVal:
                endV=lenVal
            nextLine=Text(result, text=sound.Description[startV:endV], size=10, font=self.Font, color=self.DescriptionColor, width=45)
            endV=endV+50
            startV=startV+50


    def updateTitle(self):
        if self.DescriptionValue.value!=None and self.ActiveDescription!=None and len(self.ActiveDescription)>0 and len(self.ActiveDescription)>50 and self.MarqueCount>15:
            startIndex=self.MarqueCount-15
            endIndex=startIndex+50
            if endIndex>len(self.ActiveDescription):
                if endIndex>len(self.ActiveDescription)+15:
                    self.DescriptionValue.value=self.ActiveDescription[0:50]
                    self.MarqueCount=0
                    return
                else:
                    self.MarqueCount=self.MarqueCount+1
                    return
            self.DescriptionValue.value=self.ActiveDescription[startIndex:endIndex]
        self.MarqueCount=self.MarqueCount+1
        
            

            



    def updateTime(self):
        timeNow=datetime.datetime.now()
        month= timeNow.month
        day=timeNow.day
        year=timeNow.year
        hour=timeNow.hour
        minute=timeNow.minute
        if len(str(minute))<2:
            minute="0"+str(minute)
        tz="AM"
        if hour>12:
            tz="PM"
            hour=hour-12
        if len(str(hour))<2:
            hour="0"+str(hour)
        theDate=str(month)+"/"+str(day)+"/"+str(year)+" "+str(hour)+":"+str(minute)+" "+tz
        self.WelcomeMessage.value=theDate
        if timeNow.minute==0:
            self._updateTemp()
            self._loadRandomSound()

    def _updateTemp():
        temp=self.WeatherRepository.GetTempF()
        if temp!=None:
            temp=str(temp)+"°F"
            color=self.WeatherRepository.GetTempColor(temp)
            self.Temperature.color=color
            self.Temperature.value=temp
               

    def loadRandomPhoto(self):
        path=self.ImgRepository.getRandomFile()
        self.ActivePhoto.image=path
        self.ActivePhoto.width=self.ImgRepository.resizeImageWidth(path, 300)



    def loadApp(self):
        self.loadRandomPhoto()
        self.updateTime()
        self.WelcomeMessage.repeat(60000, self.updateTime)
        self.ActivePhoto.repeat(20000, self.loadRandomPhoto)
        self.app.display()


c=guiController('D:\SqlLite\stillwatersounds.db','D:\FromMyDocuments\House Project Sounds',"D:\FromMyDocuments\HouseProjectPhotos","D:\FromMyDocuments\Icons","COM5", None)
c.loadApp()

