String nom = "Arduino";
String msg;
bool msgRecvd;
int televisionMode=0;
int currentTVColor=0;

int fireState=0;
int spookState=0;
int delayCounter=0;
int MaxFire=250;
int MinFire=25;
int MaxSpook=250;
int MinSpook=25;
int flameVal=0;
int spookVal=0;
// variable light pins
int tv_pin_rd= 2;
int tv_pin_grn = 3;
int tv_pin_blu = 4;
#define FIREPLACE_PIN 5
#define SPOOKY_BASEMENT_PIN 6
#define FIREPIT_PIN 7
#define WOODEXP1_PIN 8
// basement
#define BASEMENTMAIN_PIN 22
// main level
#define DEN_PIN 24
#define LIVINGROOM_PIN 28
// Vaulted
#define DINETTE_PIN 30
#define KITCHEN_PIN 31
#define DININGROOM_PIN 32
#define FOYER_PIN 33
// Outside
#define FRONTHOUSE_PIN 34
#define BACKHOUSE_PIN 35
// Upstairs
#define BEAUBED_PIN 36
#define NICOLEBED_PIN 37
#define DARKROOM_PIN 38
#define MASTERBED_PIN 39
#define MASTERBATH_PIN 40

void setup() {
  Serial.begin(9600);
  pinMode(DINETTE_PIN, OUTPUT);
  pinMode(FIREPLACE_PIN, OUTPUT);
  pinMode(SPOOKY_BASEMENT_PIN, OUTPUT);
  pinMode(tv_pin_rd, OUTPUT);
  pinMode(tv_pin_grn, OUTPUT);
  pinMode(tv_pin_blu, OUTPUT);
}

void loop() {
  readSerialPort();
  delayCounter++;
  if (delayCounter>=500)
  {
    delayCounter=0;
    }
if (msgRecvd==true)
{
  if (msg != "") {
    sendData();
  }
  else {
    sendError();
    }
    delay(100);
    msgRecvd=false;
}
if (televisionMode!=0)
{
  if (televisionMode==1&&delayCounter%100==0)
  {
    RunTVMode();
    }
   if (televisionMode==2&&delayCounter%50==0)
   {
    RunTVMode();
    }
  
  }
  if (spookState>0&&delayCounter%30==0)
  {
    spook();
    }
   if (fireState>0 && delayCounter%25==0)
  {
    burn();
    }
  delay(1);

}



void readSerialPort() {
  msg = "";
  if (Serial.available()) {
    delay(10);
    while (Serial.available() > 0) {
      char myChar= (char)Serial.read();
        msgRecvd=true;
        if (isDigit(myChar))
        {
          msg += myChar;
        }
      }
    
    Serial.flush();
  }
}


void sendError() {
  //write data
  Serial.print("No number recv'd.");
}

void lampOn() {
    Serial.print("Lamp on.");
    digitalWrite(DINETTE_PIN, HIGH);
  }\

void fireOn(){
   Serial.print("Fire walk with me.");
  fireState=1;
  flameVal=random(25,50);
  setMaxFire();
  }

  void spookOn(){
   Serial.print("Spooky time.");
  spookState=1;
  spookVal=random(50,70);
  setMaxSpook();
  }
void setMaxFire()
{
  MaxFire=random(100,160);
  MinFire=random(11,50);
}

void setMaxSpook()
{
  MaxSpook=random(120,220);
  MinSpook=random(5,30);
  }
  
void burn() {
   if (fireState==1)
   {
     flameVal=flameVal+2;
     if (flameVal>MaxFire)
     {
       setMaxFire();
       Serial.print("Going down");
       fireState=2;
      } 
    }
    else {
      flameVal=flameVal-2;
      if (flameVal<MinFire)
      {
        Serial.print("Going up");
        setMaxFire();
        fireState=1;
      }
      
      }
      analogWrite(FIREPLACE_PIN, flameVal);
    
  }

  void spook() {
   if (spookState==1)
   {
     spookVal=spookVal+1;
     if (spookVal>MaxSpook)
     {
       setMaxSpook();
       Serial.print("Going spook down");
       spookState=2;
      } 
    }
    else {
      spookVal=spookVal-1;
      if (spookVal<MinSpook)
      {
        Serial.print("Going up");
        setMaxSpook();
        spookState=1;
      }
      
      }
      analogWrite(SPOOKY_BASEMENT_PIN, spookVal);
    
  }



void lampOff() {
      Serial.print("Lamp off.");
    digitalWrite(DINETTE_PIN, LOW);
      analogWrite(tv_pin_rd, 0);
  analogWrite(tv_pin_grn, 0);
  analogWrite(tv_pin_blu, 0);
  analogWrite(FIREPLACE_PIN, 0);
  analogWrite(SPOOKY_BASEMENT_PIN, 0);
  televisionMode=0;
  currentTVColor=0;
  spookVal=0;
  spookState=0;
  flameVal=0;
  fireState=0;
  }

void sendData() {
  //write data
  Serial.print(nom);
  Serial.print(" received : ");
  Serial.print(msg);
  if (msg=="0")
  {
    lampOff();
    }
  if (msg=="1")
  {
    lampOn();
    }
  if (msg=="3")
  {
   
    SetTVMode(false);
   }
   if (msg=="4")
  {
   
    SetTVMode(true);
   }
      if (msg=="5")
  {
   
    fireOn();
   }

     if (msg=="6")
  {
   
    spookOn();
   }
}


// TV Code:
void SetTVMode(bool videoGame)
{
  if (videoGame==true)
  {
      Serial.print("Video Game Mode!");
    televisionMode=2;
  }
  else {
    
  Serial.print("TV Mode!");
  televisionMode=1;
  }
  
}

void RunTVMode()
{
  int randomNum = random(0,100);
  if (randomNum>80||currentTVColor==0)
  {
    currentTVColor=random(1,8);
    SetColor(currentTVColor);
    }
     
}
// 1= red, 2=green 3= blue 4 raspberry 5=cyan 6=magenta 7=yellow 8=white
void SetColor(int type)
{
        Serial.print("Set color:");
        Serial.print(type);
  switch (type) {
  case 1:
    RGB_color(255, 0, 0); // Red
    break;
  case 2:
    RGB_color(0, 255, 0); // Green
    break;
  case 3:
    RGB_color(0, 0, 255); // Blue
    break;
  case 4:
    RGB_color(255, 255, 125); // Rasp
    break;
  case 5:
    RGB_color(0, 255, 255); // Cyan
    break;
  case 6:
    RGB_color(255, 0, 255); // Magenta
    break;
  case 7:
    RGB_color(255, 255, 0); // Yellow
    break;
 case 8:
    RGB_color(255, 255, 255); // White
    break;
  default:
    break;
  }
}

void RGB_color(int red_light_value, int green_light_value, int blue_light_value)
 {
  analogWrite(tv_pin_rd, red_light_value);
  analogWrite(tv_pin_grn, green_light_value);
  analogWrite(tv_pin_blu, blue_light_value);
}
