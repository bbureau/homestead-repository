class Floor:
    def __init__(self, row):
         self.ID=row[0]
         self.Name=row[1]
         self.Description=row[2]
         self.Icon=row[3]
         self.MapFile=row[4]
         self.IsRoom=row[5]
class Room:
    def __init__(self, row):
         self.ID=row[0]
         self.Code=row[1]
         self.Name=row[2]
         self.ExplanationPath=row[3]
         self.FloorID=row[4]
         self.ArduinoCode=row[5]
         self.SecondaryCode=row[6]
         self.SecondaryIcon=row[7]
         self.SecondaryInactiveIcon=row[8]
         self.TertiaryCode=row[9]
         self.TertiaryIcon=row[10]
         self.TertiaryInactiveIcon=row[11]