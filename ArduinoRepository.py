import serial,time
from serial import Serial 
class ArduinoRepository:
    def __init__(self, comPort, backUp):
         self.IsConnected=False
         self.Comport=comPort
         self.BackupPort=None
         self.BackupPortIndex=0
         if backUp!=None:
             if ',' in backUp:
                 self.BackupPort=backUp.split(',')
             else:
                 self.BackupPort=[backUp]


         self.BackupPortIndex=0
         self.Connect()

    def __del__(self):
        self.arduino.close()
    
    def Connect(self):
        if self.IsConnected==False:
            try:
                self.arduino=serial.Serial(self.Comport, 9600, timeout=1)
                print("{} connected!".format(self.arduino.port))
                self.IsConnected=True

            except: 
                print("error connecting on {}!".format(self.Comport))
                if self.BackupPort!=None and len(self.BackupPort)<self.BackupPortIndex:
                    self.Comport=self.BackupPort(self.BackupPortIndex)
                    self.BackupPortIndex=self.BackupPortIndex+1
                    self.Connect()
                

    def GetSerialData(self):
        self.Connect()
        if  self.arduino.inWaiting()>0: 
            answer=self.arduino.readline()
            print(answer)
            self.arduino.flushInput() #remove data after readinghi
            
    def SendSerialData(self, cmd):
        self.Connect
        if self.IsConnected==False:
            return
        time.sleep(0.1) #wait for serial to open
        if self.arduino.isOpen():
            try:
                print("{} command!".format(cmd))
                comm=str(cmd)
                self.arduino.write(comm.encode())
            except Exception as e: 
                t=e
    