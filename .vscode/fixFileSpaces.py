import sqlite3
import os
from os.path import exists

conn = sqlite3.connect('D:\SqlLite\stillwatersounds.db')
c = conn.cursor()
command="Select Filename as a from Photos Where FileName like '% %'"
c.execute(command)
rows=c.fetchall()
for row in rows:
    val=row[0]
    replVal=val.replace(" ","")
    
    path="D:\FromMyDocuments\HouseProjectPhotos\\"+val
    newpath=path.replace(" ","")
    os.rename(path,newpath)
    command="Update Photos set Filename='"+replVal+"' Where Filename='"+val+"'"
    c.execute(command)
    conn.commit()

command="Select Filename, Folder from SoundFiles Where FileName like '% %'"
c.execute(command)
rows=c.fetchall()
for row in rows:
    fn=row[0]
    folder=row[1]
    replVal=fn.replace(" ","")
    
    path="D:\FromMyDocuments\House Project Sounds\\"+folder+"\\"+fn
    newpath="D:\FromMyDocuments\House Project Sounds\\"+folder+"\\"+fn.replace(" ","")
    fileExists=exists(newpath)
    if fileExists is False:
        os.rename(path,newpath)
    command="Update SoundFiles set Filename='"+replVal+"' Where Filename='"+fn+"'"
    c.execute(command)
    conn.commit()

conn.close()