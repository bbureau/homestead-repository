from dateRepository import *
from fileRepository import *
from FloorRepository import *
from guizero import *
from RoomRepository import *
from ArduinoRepository import *

class HomeController:
    def __init__(self, dbConn, imgPath,iconPath, parent, arduinoCode, arduinoRepo):
                self.ImgPath=imgPath
                self.DbConn=dbConn
                self.IconPath=iconPath
                self.ArduinoRepository=arduinoRepo
                self.Parent=parent
                self.TitleColor="black"
                self.Font="Arial Black"
                self.FloorController=FloorController(dbConn, imgPath, iconPath, parent, arduinoCode, arduinoRepo)
                self.FloorRepo=FloorRepository(dbConn)
                self.MainWindow=Window(self.Parent,title="Explore the Homestead")
                MainTitle=Text(self.MainWindow, text="Please Select a Region", size=16, font=self.Font, color=self.TitleColor)
                box=Box(self.MainWindow)
                floors=self.FloorRepo.LoadAllFloors()
                for floor in floors:
                        args=[]
                        args.append(floor.ID)
                        btn=PushButton(box, command=self.FloorController.LoadFloor, image=iconPath+"//"+floor.Icon, height=50, width=50, align="left", args=args, text=floor.Name)

class FloorController:
        def __init__(self, dbConn, imgPath,iconPath, parent, arduinoCode, arduinoRepo):
                self.ImgPath=imgPath
                self.Font="Arial Black"
                self.TitleColor="black"
                self.DbConn=dbConn
                self.ImagePath=imgPath
                self.ArduinoRepository=arduinoRepo
                self.IconPath=iconPath
                self.Parent=parent
                self.FloorRepo=FloorRepository(dbConn)
                self.RoomRepo=RoomRepository(dbConn)
                self.RoomController=RoomController(dbConn, imgPath, iconPath, parent, arduinoCode, arduinoRepo)
        def LoadFloor(self, id):
                floor=self.FloorRepo.LoadFloorByID(id)
                if floor==None:
                        return
                window=Window(self.Parent)
                if floor.MapFile!=None and len(floor.MapFile)>0:
                        photo=Picture(window, image=self.IconPath+"\\"+floor.MapFile)
                if floor.IsRoom==1:
                        self.RoomController.LoadRoomByFloorId(id)
                        return
                rooms=self.RoomRepo.LoadRoomsByFloorID(id)
                text=Text(window, text=floor.Name+ "- Please Select a Room",size=16, font=self.Font, color="lightblue" )
                box=Box(window)
                for room in rooms:
                        label=room.Code
                        args=[]
                        args.append(room.ID)
                        btn=PushButton(box, command=self.RoomController.LoadRoomById, args=args, text=label, align="left")
                window.show()
                
                
class FlipBtn:
        def __init__(self, dbConn, iconPath, parent, arduinoRepo, arduinoCode, icon, inactiveicon):
                self.DbConn=dbConn
                self.IconPath=iconPath
                self.ArduinoRepository=arduinoRepo
                self.Parent=parent
                self.RoomRepo=RoomRepository(dbConn)
                self.LightState=0
                self.CurrentIterator=0
                self.ArduinoCode=arduinoCode
                self.ButtonSize=25
                self.ActiveIcon=icon
                self.InactiveIcon=inactiveicon
                self.Button=PushButton(parent, height=self.ButtonSize, width=self.ButtonSize, command=self.Flip, image=self.IconPath+"\\"+self.ActiveIcon, align="left")
        def Flip(self):
                if (self.ArduinoCode==None):
                        return
                if (self.LightState==0):
                        self.LightState=1
                        self.ArduinoRepository.SendSerialData(self.ArduinoCode)
                        self.Button.image=self.IconPath+"\\"+self.InactiveIcon
                else:
                        self.LightState=0
                        self.ArduinoRepository.SendSerialData(0)
                        self.Button.image=self.IconPath+"\\"+self.ActiveIcon
                self.Button.height=self.ButtonSize
                self.Button.width=self.ButtonSize
        
        


class RoomController:
        def __init__(self, dbConn, imgPath,iconPath, parent, arduinoCode, arduinoRepo):
                self.ImgPath=imgPath
                self.DbConn=dbConn
                self.ImagePath=imgPath
                self.IconPath=iconPath
                self.ArduinoRepository=arduinoRepo
                self.Parent=parent
                self.RoomRepo=RoomRepository(dbConn)
                self.ImgRepository=FileRepository(imgPath)
                self.ActivePhoto=None
                self.ActivePhotoDesc=None
                self.StoryBase=None
                self.ActiveID=None
                self.BaseDesc=None
                self.Story=None
                self.LightState=0
                self.LightState2=0
                self.LightState3=0
                self.List2=None
                self.LightButton=None
                self.CurrentIterator=0
                self.Room=None
                self.ArduinoCode=None
                self.SecondaryBtn=None
                self.TertiaryBtn=None
                self.Font="Arial Black"
                self.TitleColor="black"
                self.RoomList=[]
        def LoadRoomByFloorId(self, id):
                room=self.RoomRepo.LoadRoomByFloorID(id)
                self.LoadRoom(room)
        def LoadRoomById(self, id):
                room=self.RoomRepo.LoadRoomByRoomID(id)
                self.LoadRoom(room)
        def LoadRoom(self, room):
                self.PurgeList()
                self.ArduinoCode=room.ArduinoCode
                self.Room=room
                window=Window(self.Parent, width=800, height=500)
                box=Box(window)
                
                if (room.ArduinoCode!=None):
                        MainTitle=Text(box, text=room.Name, size=16, font=self.Font, color=self.TitleColor, align="left")
                        self.LightButton=PushButton(box, height=25, width=25, command=self.FlipLights, image=self.IconPath+"\\noun_Idea_1175577.png", align="left")
                else:
                        MainTitle=Text(box, text=room.Name, size=16, font=self.Font, color=self.TitleColor)
                if (room.SecondaryCode!=None and room.SecondaryIcon!=None and room.SecondaryInactiveIcon!=None):
                        self.SecondaryBtn=FlipBtn(self.DbConn, self.IconPath, box, self.ArduinoRepository, room.SecondaryCode, room.SecondaryIcon, room.SecondaryInactiveIcon)
                if (room.TertiaryCode!=None and room.TertiaryIcon!=None and room.TertiaryInactiveIcon!=None):
                        self.SecondaryBtn=FlipBtn(self.DbConn, self.IconPath, box, self.ArduinoRepository, room.TertiaryCode, room.TertiaryIcon, room.TertiaryInactiveIcon)
   
                        
                self.ActiveID=room.ID 
                self.ActivePhoto=Picture(window, height=200)
                self.ActivePhotoDesc=Text(window, size=10, font=self.Font, color="lightblue")
                self.StoryBase=RoomImg(room.ExplanationPath, "")
                self.StoryBase.Iterator=0
                self.Story=Picture(window, image=self.IconPath+"\\"+room.ExplanationPath) 
                self.loadRandomPhoto()
                self.ActivePhoto.repeat(5000,self.loadRandomPhoto)
                self.Story.repeat(5000, self.loadStoryLine)
                self.RoomList.append(window)
        def PurgeList(self):
                if len(self.RoomList)>0:
                        for i in self.RoomList:
                                i.destroy()
                self.ArduinoRepository.SendSerialData(0)
                self.RoomList=[]


        def FlipLights(self):
                if (self.ArduinoCode==None):
                        return
                if (self.LightState==0):
                        self.LightState=1
                        self.ArduinoRepository.SendSerialData(self.ArduinoCode)
                        self.LightButton.image=self.IconPath+"\\noun_Light_1570737.png"
                        self.LightButton.height=25
                        self.LightButton.width=25
                else:
                        self.LightState=0
                        self.ArduinoRepository.SendSerialData(0)
                        self.LightButton.image=self.IconPath+"\\noun_Idea_1175577.png"
                        self.LightButton.height=25
                        self.LightButton.width=25
        

        def loadStoryLine(self):
                nextImg=self.RoomRepo.GetNextStoryInSeries(self.ActiveID, self.CurrentIterator, self.StoryBase)
                self.CurrentIterator=nextImg.Iterator
                self.Story.image=self.IconPath+"\\"+nextImg.Path

        def loadRandomPhoto(self):
                img=self.RoomRepo.GetRandomRoomPhoto(self.ActiveID)
                if img==None:
                        return
                thePath=self.ImagePath+"\\"+img.Path
                self.ActivePhoto.image=thePath
                self.ActivePhoto.width=self.ImgRepository.resizeImageWidth(thePath, 200)
                self.ActivePhotoDesc.value=img.Description
