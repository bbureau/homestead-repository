import requests
import Setting
import SettingRepository
import json
class WeatherRepository:
        def __init__(self, key, settingRepo):
                self.ApiKey=key
                self.SettingRepository=settingRepo
                self._locationCode=None
                self._location=None
        def GetLocation(self):
                if self.ApiKey==None:
                        return None
                if self._location!=None:
                        return self._location
                ip = requests.get('https://checkip.amazonaws.com').text.strip()
                dbIp=self.SettingRepository.GetSettingValue('IPAddress')
                if ip==None:
                        return None
                elif  dbIp!=ip:
                        setting=Setting.Setting('IPAddress', 'Last logged IP Address',  ip)
                        self.SettingRepository.UpsertSetting(setting)
                else:
                        dbVal=self.SettingRepository.GetSettingValue('Location')
                        if dbVal!=None:
                                self._location=dbVal
                                return self._location
                locResponse = requests.get('https://ipwhois.app/json/'+ip)
                if locResponse==None:
                        return None
                location=locResponse.json()
                if location==None or location["city"]==None or location["region"]==None:
                        return None
                result=location["city"]+","+location["region"]
                setting=Setting.Setting('Location',"Users current locaion", result)
                self.SettingRepository.UpsertSetting(setting)
                self._location=result
                return self._location

        def _UpdateRequired(self):
                ip = requests.get('https://checkip.amazonaws.com').text.strip()
                dbIp=self.SettingRepository.GetSettingValue('IPAddress')
                if dbIp==None:
                        return 1
                elif  dbIp!=ip:
                        setting=Setting.Setting('IPAddress', 'Last logged IP Address',  dpIp)
                        self.SettingRepository.UpsertSetting(setting) 
                        return 0
                else: 
                        return 0

        def GetLocationCode(self):
                if self.ApiKey==None:
                        return None
                if self._locationCode!=None:
                        return self._locationCode
                if self._UpdateRequired()==0:
                        dbVal=self.SettingRepository.GetSettingValue('WeatherAPILocCode')
                        if dbVal!=None:
                                self._locationCode=dbVal
                                return self._locationCode
                location=self.GetLocation()
                if location==None:
                        return None
                locResponse=requests.get("http://dataservice.accuweather.com/locations/v1/cities/search?apikey="+self.ApiKey+"&q="+location)
                if locResponse==None:
                        return None
                locJson=locResponse.json()
                if locJson==None or locJson[0]["Key"]==None:
                        return None
                self._locationCode=locJson[0]["Key"]
                setting=Setting.Setting('WeatherAPILocCode', 'Weather API Loc Code',  self._locationCode)
                self.SettingRepository.UpsertSetting(setting) 
                return self._locationCode
        
        def GetTempColor(self, temp):
                if temp==None or not (isinstance(temp, int)) or (temp>59 and temp<84):
                        return "black"
                if temp>95:
                        return "red"
                elif temp>85:
                        return "sienna2"
                elif temp<0:
                        return "blue"
                return "skyblue1"
                

        def GetTempF(self):
                if self.ApiKey==None:
                                return None
                code=self.GetLocationCode()
                if code==None:
                        return None
                response=requests.get("http://dataservice.accuweather.com/currentconditions/v1/"+code+"?apikey="+self.ApiKey)
                if response==None:
                        return None
                locJson=response.json()
                temp= locJson[0]["Temperature"]["Imperial"]["Value"]
                return int(temp)
