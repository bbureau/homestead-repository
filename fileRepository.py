import os
import math
from random import *
import PIL
from PIL import Image
class FileRepository:
    def __init__(self, folder):
        self.SourceFolder=folder
    def getFileList(self, path):
        if path!=None and len(path)>0:
            return os.listdir(path)
        return os.listdir(self.SourceFolder)

    def resizePhoto(self, photo, maxEdgeSize):
        newHeight=0
        newWidth=0
        if photo.width>photo.height:
            mult=photo.height/photo.width
            newWidth=maxEdgeSize
            newHeight=math.floor(maxEdgeSize*mult)
        else:
            mult=photo.width/photo.height
            newHeight=maxEdgeSize
            newWidth=math.floor(maxEdgeSize*mult)
        image=photo.resize(newWidth, newHeight)
    
    def resizeImageWidth(self, path, height):
        img = Image.open(path)
        mult=img.width/img.height
        return math.floor(height*mult)
        


    def getRandomFile(self):
        allFiles=self.getFileList(None)
        if allFiles==None or len(allFiles)==0:
            return None
        roll=randint(0, (len(allFiles)-1))
        return self.SourceFolder+"\\"+ allFiles[roll]
