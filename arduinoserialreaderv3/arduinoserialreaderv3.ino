String nom = "Arduino";
String msg;
bool msgRecvd;
int televisionMode=0;
int currentTVColor=0;

int fireState=0;
int spookState=0;
int delayCounter=0;
int MaxFire=250;
int MinFire=25;
int MaxSpook=250;
int MinSpook=25;
int flameVal=0;
int spookVal=0;
// variable light pins
int tv_pin_rd= 2;
int tv_pin_grn = 3;
int tv_pin_blu = 4;
#define FIREPLACE_PIN 5
#define SPOOKY_BASEMENT_PIN 6
#define FIREPIT_PIN 7
#define WOODEXP1_PIN 8
// basement
#define BASEMENTMAIN_PIN 22
// main level
#define DEN_PIN 24
#define LIVINGROOM_PIN 42
// Vaulted
#define DINETTE_PIN 30
#define KITCHEN_PIN 9
#define LAUNDRY_PIN 42
#define DININGROOM_PIN 32
#define FOYER_PIN 33
// Outside
#define FRONTHOUSE_PIN 34
#define BACKHOUSE_PIN 35
// Upstairs
#define BEAUBED_PIN 36
#define NICOLEBED_PIN 37
#define DARKROOM_PIN 38
#define MASTERBED_PIN 39
#define MASTERBATH_PIN 40
#define TREEHOUSE_PIN 41


void setup() {
  Serial.begin(9600);
  pinMode(DINETTE_PIN, OUTPUT);
  pinMode(FIREPIT_PIN, OUTPUT);
  pinMode(WOODEXP1_PIN, OUTPUT);
  pinMode(LIVINGROOM_PIN, OUTPUT);
  pinMode(DEN_PIN, OUTPUT);
  pinMode(DININGROOM_PIN, OUTPUT);
   pinMode(FOYER_PIN, OUTPUT);
    pinMode(FRONTHOUSE_PIN, OUTPUT);
    pinMode(BACKHOUSE_PIN, OUTPUT);
    pinMode(BEAUBED_PIN, OUTPUT);
    pinMode(NICOLEBED_PIN, OUTPUT);
     pinMode(DARKROOM_PIN, OUTPUT);
          pinMode(KITCHEN_PIN, OUTPUT);
     pinMode(MASTERBED_PIN, OUTPUT);
     pinMode(MASTERBATH_PIN, OUTPUT);
       pinMode(TREEHOUSE_PIN, OUTPUT);
    pinMode(BASEMENTMAIN_PIN, OUTPUT);
  pinMode(FIREPLACE_PIN, OUTPUT);
  pinMode(SPOOKY_BASEMENT_PIN, OUTPUT);
  pinMode(tv_pin_rd, OUTPUT);
  pinMode(tv_pin_grn, OUTPUT);
  pinMode(tv_pin_blu, OUTPUT);
}

void loop() {
  readSerialPort();
  delayCounter++;
  if (delayCounter>=500)
  {
    delayCounter=0;
    }
if (msgRecvd==true)
{
  if (msg != "") {
    sendData();
  }
  else {
    sendError();
    }
    delay(100);
    msgRecvd=false;
}
if (televisionMode!=0)
{
  if (televisionMode==1&&delayCounter%100==0)
  {
    RunTVMode();
    }
   if (televisionMode==2&&delayCounter%50==0)
   {
    RunTVMode();
    }
  
  }
  if (spookState>0&&delayCounter%30==0)
  {
    spook();
    }
   if (fireState>0 && delayCounter%25==0)
  {
    burn();
    }
  delay(1);

}



void readSerialPort() {
  msg = "";
  if (Serial.available()) {
    delay(10);
    while (Serial.available() > 0) {
      char myChar= (char)Serial.read();
        msgRecvd=true;
        if (isDigit(myChar))
        {
          msg += myChar;
        }
      }
    
    Serial.flush();
  }
}


void sendError() {
  //write data
  Serial.print("No number recv'd.");
}

void lampOn(int pin) {
    Serial.print(pin);
    Serial.print("Lamp on.");
    digitalWrite(pin, HIGH);
  }\

void fireOn(){
   Serial.print("Fire walk with me.");
  fireState=1;
  flameVal=random(25,50);
  setMaxFire();
  }

  void spookOn(){
   Serial.print("Spooky time.");
  spookState=1;
  spookVal=random(50,70);
  setMaxSpook();
  }
void setMaxFire()
{
  MaxFire=random(100,160);
  MinFire=random(11,50);
}

void setMaxSpook()
{
  MaxSpook=random(120,220);
  MinSpook=random(5,30);
  }
  
void burn() {
   if (fireState==1)
   {
     flameVal=flameVal+2;
     if (flameVal>MaxFire)
     {
       setMaxFire();
       Serial.print("Going down");
       fireState=2;
      } 
    }
    else {
      flameVal=flameVal-2;
      if (flameVal<MinFire)
      {
        Serial.print("Going up");
        setMaxFire();
        fireState=1;
      }
      
      }
      analogWrite(FIREPLACE_PIN, flameVal);
    
  }

  void spook() {
   if (spookState==1)
   {
     spookVal=spookVal+1;
     if (spookVal>MaxSpook)
     {
       setMaxSpook();
       Serial.print("Going spook down");
       spookState=2;
      } 
    }
    else {
      spookVal=spookVal-1;
      if (spookVal<MinSpook)
      {
        Serial.print("Going up");
        setMaxSpook();
        spookState=1;
      }
      
      }
      analogWrite(SPOOKY_BASEMENT_PIN, spookVal);
    
  }



void lampOff() {
  digitalWrite(DINETTE_PIN, LOW);
  digitalWrite(FIREPIT_PIN, LOW);
  digitalWrite(WOODEXP1_PIN, LOW);
  digitalWrite(LIVINGROOM_PIN, LOW);
  digitalWrite(DEN_PIN, LOW);
  digitalWrite(DININGROOM_PIN, LOW);
   digitalWrite(FOYER_PIN, LOW);
    digitalWrite(FRONTHOUSE_PIN, LOW);
    digitalWrite(BACKHOUSE_PIN, LOW);
    digitalWrite(BEAUBED_PIN, LOW);
    digitalWrite(NICOLEBED_PIN, LOW);
     digitalWrite(DARKROOM_PIN, LOW);
     digitalWrite(MASTERBED_PIN, LOW);
     digitalWrite(MASTERBATH_PIN, LOW);
     digitalWrite(MASTERBATH_PIN, LOW);
       digitalWrite(LAUNDRY_PIN, LOW);
    digitalWrite(BASEMENTMAIN_PIN, LOW);
  analogWrite(FIREPLACE_PIN, 0);
  digitalWrite(KITCHEN_PIN, LOW);
  analogWrite(KITCHEN_PIN, 0);
  analogWrite(SPOOKY_BASEMENT_PIN, 0);
  analogWrite(tv_pin_rd, 0);
  analogWrite(tv_pin_grn, 0);
  analogWrite(tv_pin_blu, 0);

  televisionMode=0;
  currentTVColor=0;
  spookVal=0;
  spookState=0;
  flameVal=0;
  fireState=0;
  }

void sendData() {
  //write data
  Serial.print(nom);
  Serial.print(" received : ");
  Serial.print(msg);
  if (msg=="0")
  {
    lampOff();
    }
  if (msg=="1")
  {
    analogWrite(KITCHEN_PIN, 200);
    return;
    }
  if (msg=="2")
  {
    lampOn(DININGROOM_PIN);
    }
  if (msg=="3")
  {
   
    SetTVMode(false);
   }
   if (msg=="4")
  {
   
    SetTVMode(true);
   }
      if (msg=="5")
  {
   
    fireOn();
   }
         if (msg=="5")
  {
   
    fireOn();
   }
   if (msg=="6")
   {
     lampOn(FOYER_PIN);
    }
    if (msg=="7")
    {
      lampOn(DINETTE_PIN);
    }
    if (msg=="8")
    {
      lampOn(BACKHOUSE_PIN);
      }
      if (msg=="9")
      {
        lampOn(FRONTHOUSE_PIN);
        }
      if (msg=="10")
      {
        lampOn(DEN_PIN);
        }
      if (msg=="11")
      {
        lampOn(BASEMENTMAIN_PIN);
        }
      if (msg=="12")
      {
        lampOn(DARKROOM_PIN);
        }
       if (msg=="14")
       {
        lampOn(MASTERBED_PIN);
        }
        if (msg=="15")
        {

          lampOn(BEAUBED_PIN);
          }
                   if (msg=="16")
        {

          lampOn(MASTERBATH_PIN);
        }
          if (msg=="17")
          {
            lampOn(NICOLEBED_PIN);
            }
            if (msg=="18")
          {
            lampOn(FIREPIT_PIN);
            }
           if (msg=="19")
          {
            lampOn(TREEHOUSE_PIN);
          }

           if (msg=="21")
          {
            lampOn(WOODEXP1_PIN);
          }

          if (msg=="25")
          {
            lampOn(LIVINGROOM_PIN);
            }

          if (msg=="25")
          {
            lampOn(LAUNDRY_PIN);
            }


     if (msg=="26")
  {
   
    spookOn();
   }
}


// TV Code:
void SetTVMode(bool videoGame)
{
  if (videoGame==true)
  {
      Serial.print("Video Game Mode!");
    televisionMode=2;
  }
  else {
    
  Serial.print("TV Mode!");
  televisionMode=1;
  }
  
}

void RunTVMode()
{
  int randomNum = random(0,100);
  if (randomNum>80||currentTVColor==0)
  {
    currentTVColor=random(1,8);
    SetColor(currentTVColor);
    }
     
}
// 1= red, 2=green 3= blue 4 raspberry 5=cyan 6=magenta 7=yellow 8=white
void SetColor(int type)
{
        Serial.print("Set color:");
        Serial.print(type);
  switch (type) {
  case 1:
    RGB_color(255, 0, 0); // Red
    break;
  case 2:
    RGB_color(0, 255, 0); // Green
    break;
  case 3:
    RGB_color(0, 0, 255); // Blue
    break;
  case 4:
    RGB_color(255, 255, 125); // Rasp
    break;
  case 5:
    RGB_color(0, 255, 255); // Cyan
    break;
  case 6:
    RGB_color(255, 0, 255); // Magenta
    break;
  case 7:
    RGB_color(255, 255, 0); // Yellow
    break;
 case 8:
    RGB_color(255, 255, 255); // White
    break;
  default:
    break;
  }
}

void RGB_color(int red_light_value, int green_light_value, int blue_light_value)
 {
  analogWrite(tv_pin_rd, red_light_value);
  analogWrite(tv_pin_grn, green_light_value);
  analogWrite(tv_pin_blu, blue_light_value);
}
