import Setting
import sqlite3
class SettingRepository:
    def __init__(self, dbConn):
        self.dbConnection=dbConn   
    def GetSettingValue(self, key):
        conn = sqlite3.connect(self.dbConnection)
        c = conn.cursor()
        command="Select * from SETTINGS Where Name='"+key+"' LIMIT 1"
        c.execute(command)
        rows=c.fetchall()
        if len(rows)==0:
            return None
        return Setting.Setting(rows[0])

    def UpsertSetting(self, setting):
        conn = sqlite3.connect(self.dbConnection)
        if setting==None or setting.Name==None or setting.Value==None:
            return
        c = conn.cursor()
        command="Select * from SETTINGS Where Name='"+setting.Name+"' LIMIT 1"
        c.execute(command)
        rows=c.fetchall()
        if len(rows)==0:
            insertCommand="insert into SETTINGS (NAME, DESCRIPTION, VALUE) VALUES ('"+setting.Name+"','"+setting.Description+"','"+setting.Value+"')"
            c.execute(insertCommand)
            return
        updateCommand="update SETTINGS SET Value='"+setting.Value+"', Description='"+setting.Description+"', Value='"+setting.Value+"' WHERE Name='"+setting.Name+"'"
        c.execute(updateCommand)
        conn.commit()
        conn.close()




    