String nom = "Arduino";
String msg;
bool msgRecvd;

void setup() {
  Serial.begin(9600);
}

void loop() {
  readSerialPort();

if (msgRecvd==true)
{
  if (msg != "") {
    sendData();
  }
  else {
    sendError();
    }
    delay(100);
    msgRecvd=false;
}

}

void readSerialPort() {
  msg = "";
  if (Serial.available()) {
    delay(10);
    while (Serial.available() > 0) {
      char myChar= (char)Serial.read();
        msgRecvd=true;
        if (isDigit(myChar))
        {
          msg += myChar;
        }
      }
    
    Serial.flush();
  }
}

void sendError() {
  //write data
  Serial.print("No number recv'd.");
}


void sendData() {
  //write data
  Serial.print(nom);
  Serial.print(" received : ");
  Serial.print(msg);
}
