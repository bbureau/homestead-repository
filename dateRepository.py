import datetime
from random import *
import sqlite3
import numpy as np
class SeasonBase: 
    def __init__(self, id, theName, theStart, theEnd, hWeight, hApiName):
         self.ID=id
         self.Name=theName
         self.StartDate=theStart
         self.EndDate=theEnd
         self.HolidayWeight=hWeight
         self.HolidayApiName=hApiName

class SoundLog:
    def __init__(self, row):
         self.ID=row[0]
         self.SoundFileID=row[1]
         self.Date=row[2]

class HouseSound:
    def __init__(self, row):
         self.ID=row[0]
         self.FileTypeID=row[1]
         self.Name=row[17]
         self.Folder=row[2]
         self.FileName=row[3]
         self.NextRunDate=row[8]
         self.MinBetweenRounds=row[9]
         self.Stars=row[11]
         self.Description=row[12]
         self.AirDate=row[14]
         self.Weight=row[13]
         self.HouseConfigId=row[7]
         self.HouseConfigLeadTime=row[18]
         self.UseNativeMonthAndDay=row[19]
         self.CustomVolume=row[20]
         self.DontRunNextIds=row[21]
         self.Authenticity=row[22]
         self.SeasonID=row[6]
         self.HouseConfigCodes=row[23]
class Season:
    def __init__(self, row):
        self.ID=row[0]
        self.Name=row[1]
        self.Weight=row[2]
        self.StartDate=row[3]
        self.EndDate=row[4]
        self.ApiCode=row[5]
class dateRepo:
        def __init__(self, dbConn):
         self.dbConnection=dbConn

        def setNewSeasons():
            conn = sqlite3.connect(self.dbConnection)
            c = conn.cursor()
            currentDateTime = datetime.datetime.now()
            date = currentDateTime.date()
            year = date.year
            pastYear=year-1
            command="Select Top 1 from Seasons WHERE ID=1"
            c.execute(command)
            rows=c.fetchall()
            if len(rows)==0:
                return None
            row=rows[0]
            currentStartYear=row[3][0:4]
            currentEndYear=row[4][0:4]
            command="Update Seasons Set StartDate=REPLACE('"+currentEndYear+"',StartDate,'"+year+"') WHERE StartDate Like '"+currentEndYear+"%'"
            c.execute(command)
            command="Update Seasons Set StartDate=REPLACE('"+currentStartYear+"',StartDate,'"+pastYear+"') WHERE StartDate Like '"+currentStartYear+"%'"
            c.execute(command)
            command="Update Seasons Set EndDate=REPLACE('"+currentEndYear+"',EndDate,'"+year+"') WHERE EndDate LIKE'"+currentEndYear+"%'"
            c.execute(command)
            conn.commit()

        def getWeekDayIds(self):
            weekday=datetime.datetime.today().weekday()
            results=[1]
            if weekday>5:
                results.append(3)
            if weekday<=5:
                results.append(2)
            results.append(4+weekday)
            return results
        def loadAllSeasons(self):
            conn = sqlite3.connect(self.dbConnection)
            c = conn.cursor()
            command="Select * from Seasons"
            c.execute(command)
            rows=c.fetchall()
            if len(rows)==0:
                return None
            result=[]
            for row in rows:
                entry=Season(row)
                result.append(entry)
            return result
        def getSeasonForId(self, seasons, id):
            for season in seasons:
                if season.ID==id:
                    return season
            return None


        def getCurrentSeason(self):
            current_time = datetime.datetime.today()  
            month=str(current_time.month)
            if len(month)<2:
                month='0'+str(month)

            day=str(current_time.day)
            if len(day)<2:
                day='0'+str(day)
            year=current_time.year
            theDate=str(year)+'-'+str(month)+'-'+str(day)

            conn = sqlite3.connect(self.dbConnection)
            c = conn.cursor()
            command="Select ID, Name, StartDate, EndDate, HolidayWeight, HolidayApiCode FROM Seasons WHERE date(StartDate)<=date('"+theDate+"') AND date(EndDate)>=date('"+theDate+"') "
            c.execute(command)
            rows=c.fetchall()
            result=[]
            for row in rows:
                entry=SeasonBase(row[0], row[1], row[2],row[3], row[4], row[5])
                result.append(entry)
            return result

class soundRepo:
    def __init__(self, dbConn):
         self.dbConnection=dbConn
         self.DateRepo=dateRepo(self.dbConnection)
  
    def logSound(self, sound):
        if sound==None:
            return
        
        conn = sqlite3.connect(self.dbConnection)
        c = conn.cursor()
        command="INSERT INTO SoundFileLog (SoundFileID,PlayDate) VALUES ("+str(sound.ID)+", DATE()) "
        c.execute(command)
        conn.commit()

    def incrementPlayDate(self, sound):
        if sound==None or sound.MinBetweenRounds==None or sound.MinBetweenRounds<1:
            return
        
        nextDate = datetime.datetime.now() + datetime.timedelta(minutes=sound.MinBetweenRounds)
        conn = sqlite3.connect(self.dbConnection)
        c = conn.cursor()
        command="Update SoundFiles SET  NextRunDate='"+str(nextDate)+"' where ID="+str(sound.ID)
        c.execute(command)
        conn.commit()
    
    def getDontRunNextIdsSelector(self):
        log=self._getLastLoggedSound()
        if log==None:
            return ""
        sound=getSoundFileById(log.SoundFileID)
        if sound==None or len(sound.DontRunNextIds)<2:
            return ""
        return " AND WHERE ID not in ("+sound.DontRunNextIds+")"


    
    def getSoundFileById(self, id):
        conn = sqlite3.connect(self.dbConnection)
        c = conn.cursor()
        command="Select * FROM SoundFiles WHERE ID="+id+" LIMIT 1"
        c.execute(command)
        rows=c.fetchall()
        if (rows==None or len(rows)==0):
                return None
        sound=HouseSound(rows[0])
        return sound


    def _getLastLoggedSound(self):
        conn = sqlite3.connect(self.dbConnection)
        c = conn.cursor()
        command="Select * FROM SoundFileLog ORDER BY ID DESC LIMIT 1"
        c.execute(command)
        rows=c.fetchall()
        if (rows==None or len(rows)==0):
                return None
        log=SoundLog(rows[0])




        
    def _getSoundType(self, sounds, requestedTypeId):
        if requestedTypeId!=None and (requestedTypeId==1 or requestedTypeId==2):
            return requestedTypeId
        
        hasAmbient=self.listHasType(sounds, 2)
        hasMusic=self.listHasType(sounds, 1)
        idToPlay=0
        if hasMusic==True and hasAmbient==True:
            return randint(1, 2)
        elif hasMusic==True:
             return 1
        else:
            return 2

    def setVolume(self, sound):
        conn = sqlite3.connect(self.dbConnection)
        c = conn.cursor()
        command="select DefaultVolume from FileType where ID="+str(sound.FileTypeID)
        c.execute(command)
        rows=c.fetchall()
        if (len(rows)>0):
            if sound.CustomVolume==None:
                val=rows[0][0]
                sound.CustomVolume=val*sound.CustomVolume


    def getRandomSound(self, requestedTypeId):
        sounds=self._getSoundsForSeason()
        if sounds==None:
            return None

        idToPlay=self._getSoundType(sounds, requestedTypeId)
        
        filterSounds=[]
        for sound in sounds:
            if sound.FileTypeID==idToPlay:
                filterSounds.append(sound)
        
        filterRoll=randint(0,1)
        if filterRoll==0:
            filterSounds=sorted(filterSounds, key=lambda sounds: sounds.ID)
        else:
            filterSounds=sorted(filterSounds, key=lambda sounds: sounds.Name)
  
        allSeasons=self.DateRepo.loadAllSeasons()
        weightSum=0
        for sound in filterSounds:
             theSeason=self.DateRepo.getSeasonForId(allSeasons,sound.SeasonID)
             if theSeason!=None and theSeason.Weight>0:
                sound.Weight=sound.Weight+theSeason.Weight
             weightSum=weightSum+sound.Weight
        
        pickedSum=randint(0, weightSum)
        theSum=0
        theSound=None
        for sound in filterSounds:
            theSum=theSum+sound.Weight
            if theSum>=pickedSum:
                self.setVolume(sound)
                return sound
        





    def listHasType(self, soundList, typeId):
        for sound in soundList:
            if sound.FileTypeID==typeId:
             return True
        return False
    
    def getSeasonIds(self):
        seasons=self.DateRepo.getCurrentSeason()
        seasonIds=""
        for season in seasons:
            if len(seasonIds)>0:
                seasonIds=seasonIds+","
        seasonIds=seasonIds+str(season.ID)
        seasonIds="("+seasonIds+")"
        return seasonIds

    def getRandomSoundOfType(self, type):
        sounds=self.getSoundOfType(type)
        if sounds==None:
            return None
        pickedInt=randint(0, len(sounds)-1)
        return sounds[pickedInt]

    def getSoundById(self, id):
        conn = sqlite3.connect(self.dbConnection)
        c = conn.cursor()
        command="select * from SoundFiles where ID="+str(id)
        c.execute(command)
        rows=c.fetchall()
        if (rows==None):
            return None

        return HouseSound(rows[0])


    def getSoundOfType(self, type):
        conn = sqlite3.connect(self.dbConnection)
        c = conn.cursor()
        command="select * from SoundFiles where (SeasonID IN "+self.getSeasonIds()+" OR SeasonID is NULL) AND FileTypeID="+str(type)
        c.execute(command)
        rows=c.fetchall()
        result=[]
        for row in rows:
                entry=HouseSound(row)
                result.append(entry)
        return result

    def _getSoundsForSeason(self):
        dontPlayNextIds=self.getDontRunNextIdsSelector()
        self.getSeasonIds()
        weekdays=self.DateRepo.getWeekDayIds()
        if len(weekdays)==0:
            return None
        weekDayIds=""
        for theId in weekdays:
            if len(weekDayIds)>0:
                weekDayIds=weekDayIds+","
            weekDayIds=weekDayIds+str(theId)
        weekDayIds="("+weekDayIds+")"
        now=datetime.datetime.now()
        hour=str(now.hour)
        dontPlayNextIds=self.getDontRunNextIdsSelector()

        conn = sqlite3.connect(self.dbConnection)
        c = conn.cursor()
        command="select * from SoundFiles where ((SeasonID IN "+self.getSeasonIds()+" OR SeasonID is NULL) AND (WeekDayConfigID IN "+weekDayIds+" OR WeekDayConfigID is null)) AND ((StartHour<="+hour+" AND EndHour>="+hour+") OR (EndHour<StartHour AND StartHour>"+hour+" AND EndHour>"+hour+")) AND (NextRunDate IS NULL OR NextRunDate<'"+str(now)+"')"+dontPlayNextIds
        c.execute(command)
        rows=c.fetchall()
        result=[]
        for row in rows:
                entry=HouseSound(row)
                result.append(entry)
        return result

#c=soundRepo('D:\SqlLite\stillwatersounds.db')
#d=c.getRandomSound(None)