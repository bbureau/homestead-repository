import datetime
from random import *
import sqlite3
import numpy as np
from dateRepository import *
from fileRepository import *
from Floor import *
class RoomImg:
        def __init__(self, path, desc):
         self.Path=path
         self.Description=desc
         self.Iterator=None

class RoomRepository:
    def __init__(self, dbConn):
         self.dbConnection=dbConn
         self.DateRepo=dateRepo(self.dbConnection)
    def LoadRoomsByFloorID(self, floorID):
        conn = sqlite3.connect(self.dbConnection)
        c = conn.cursor()
        command="SELECT * FROM ROOMS WHERE FloorID="+str(floorID)+" or FloorID='"+str(floorID)+"'"
        c.execute(command)
        conn.commit()
        rows=c.fetchall()
        result=[]
        for row in rows:
                entry=Room(row)
                result.append(entry)
        return result

    def LoadRoomByRoomID(self, roomID):
        conn = sqlite3.connect(self.dbConnection)
        c = conn.cursor()
        command="SELECT * FROM ROOMS WHERE ID="+str(roomID)
        c.execute(command)
        conn.commit()
        rows=c.fetchall()
        result=[]
        for row in rows:
                entry=Room(row)
                return entry
        return None

    def LoadRoomByFloorID(self, floorID):
        conn = sqlite3.connect(self.dbConnection)
        c = conn.cursor()
        command="SELECT * FROM ROOMS WHERE FloorID="+str(floorID)+" or FloorID='"+str(floorID)+"'"
        c.execute(command)
        conn.commit()
        rows=c.fetchall()
        result=[]
        for row in rows:
                entry=Room(row)
                return entry
        return None

    def GetNextStoryInSeries(self, roomID, iterator, originalFile):  
        conn = sqlite3.connect(self.dbConnection)
        c = conn.cursor()
        command="SELECT FileName, Description, Iterator FROM STORIES WHERE RoomID="+str(roomID)+" AND iterator>"+str(iterator)+ " ORDER BY ITERATOR"
        c.execute(command)
        rows=c.fetchall()
        if (rows==None or len(rows)==0):
                return originalFile
        result=[]
        for row in rows:
                img=RoomImg(row[0], row[1])
                img.Iterator=row[2]
                return img
        return originalFile

         
    def GetRandomRoomPhoto(self, roomID):
        conn = sqlite3.connect(self.dbConnection)
        c = conn.cursor()
        command="SELECT FileName, Description FROM PHOTOS WHERE RoomID="+str(roomID)  
        c.execute(command)
        rows=c.fetchall()
        if (rows==None or len(rows)==0):
            return None
        result=[]
        for row in rows:
                img=RoomImg(row[0], row[1])
                result.append(img)
        roll=randint(0, len(result)-1)
        return result[roll]


