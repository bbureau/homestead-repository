import datetime
from random import *
import sqlite3
import numpy as np
from dateRepository import *
from fileRepository import *
from Floor import Floor

class FloorRepository:
    def __init__(self, dbConn):
         self.dbConnection=dbConn
         self.DateRepo=dateRepo(self.dbConnection)
    def LoadAllFloors(self):
        conn = sqlite3.connect(self.dbConnection)
        c = conn.cursor()
        command="SELECT * FROM FLOORS"
        c.execute(command)
        conn.commit()
        rows=c.fetchall()
        result=[]
        for row in rows:
                entry=Floor(row)
                result.append(entry)
        return result
    
    def LoadFloorByID(self, id):
        conn = sqlite3.connect(self.dbConnection)
        c = conn.cursor()
        command="SELECT * FROM FLOORS WHERE ID="+str(id)
        c.execute(command)
        conn.commit()
        rows=c.fetchall()
        result=[]
        for row in rows:
                entry=Floor(row)
                return entry
        return None